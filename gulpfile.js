//======================================================================================================================
// UoS - Gulpfile
//======================================================================================================================


//=======================================================================================
//  GULP REQUIREMENTS
//=======================================================================================
var gulp = require('gulp');
var sass = require('gulp-sass');
var minifyCSS = require('gulp-minify-css');
var uglify = require('gulp-uglify');
var notify = require("gulp-notify");
var rename = require('gulp-rename');
// var concat = require('gulp-concat');
var jade = require('gulp-jade');


//=======================================================================================
// COMMON VARIABLES
//=======================================================================================

var appName           = 'Logic',

    noticeSuccessIcon   = 'http://www.tetrapods.org/common/images/Soton-S-logo.jpg',
    noticeFailureIcon   = 'http://www.personal.soton.ac.uk/dij/GR-Explorer/buttons/dolphin.gif';


//=======================================================================================
// TASK : STYLES
//=======================================================================================
gulp.task('styles', function () {
    gulp.src('./build/styles/logic.main.scss')
        .pipe(sass().on("error", errorSass))
        .pipe(rename({suffix: '.min'}))
        .pipe(minifyCSS())
        .pipe(gulp.dest('./dist/styles/'));
});



// //=======================================================================================
// // TASK : SCRIPTS
// //=======================================================================================

// gulp.task('scripts', function() {

//   return gulp.src([

//     './node_modules/html5shiv/dist/html5shiv.min.js', // HTML5 Shiv

//     './node_modules/chart.js/Chart.min.js', // Chart.js

//     './build/scripts/vendor/*.js', // All vendor scripts

//     './build/scripts/plugins/*.js', // All project scripts

//   ])

//   .pipe(concat('uos.main.min.js'))
//   .pipe(gulp.dest('./dist/scripts/'));

// });


//=======================================================================================
// IO
//=======================================================================================
gulp.task('build-templates', function() {
  var YOUR_LOCALS = {};

  // TEMPLATES
  //=======================================
  gulp.src('./build/templates/*.jade')
    .pipe(jade({
      locals: YOUR_LOCALS
    }))
    .pipe(gulp.dest('./dist/'));
});



//=======================================================================================
// ERRORS
//=======================================================================================
var errorSass = function(err) {
    notify.onError({
      title:    appName,
      icon:     noticeFailureIcon,
      message:  "Error: Cannot compile SASS.",
      sound:    "Beep"
    })(err);
    this.emit('end');
};





//=======================================================================================
// WATCHERS
//=======================================================================================

// SCRIPTS
// ===================================================
gulp.task('watch-scripts', function() {
    gulp.watch('./build/scripts/**/*.js', ['scripts']);
});


// STYLES
// ===================================================
gulp.task('watch-styles', function() {
    gulp.watch(['./build/styles/**/**/*.scss'  ], ['styles']);
});


// TEMPLATES
// ===================================================
gulp.task('watch-templates', function() {
  gulp.watch(['./build/**/*.jade'], ['build-templates']);
});

// =====================================================================================================================
// TASK : DEFAULT
// =====================================================================================================================
gulp.task('default',
    ['watch-styles','watch-scripts','watch-templates']
);